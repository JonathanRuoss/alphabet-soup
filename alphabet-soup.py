# Python 3.8.0


def importWordSearch(wordSearchFile):
    board = list()
    wordlist = list()

    with open(wordSearchFile, "r") as wordSearch:

        # separate word search size and assign to variables
        size = wordSearch.readline()
        size = size.split('x')
        totalRows = int(size[0])
        totalColumns = int(size[1])

        # iterates just for word search board
        for row in range(totalRows):

            # make list of characters in each row and add to board list
            line = wordSearch.readline()
            line = line.rstrip('\n')
            line = line.split(' ')
            board.append(line)

        # remaining lines are words to be found
        for word in wordSearch:
            word = word.rstrip('\n')
            word = word.replace(' ', '')
            wordlist.append(word)
    
    return totalRows, totalColumns, board, wordlist


def solveWordSearch(maxRows, maxColumns, board, wordlist, solvedWordlist):

    # check every character if it matches the first letter in each word
    for row in range(maxRows):
        for column in range(maxColumns):
            for wordNumber in range(len(wordlist)):

                # if first character matches, check characters surrounding to match second character in word
                if wordlist[wordNumber][0] == board[row][column]:
                    checkAround(maxRows, maxColumns, board, row, column, wordlist[wordNumber], wordNumber, solvedWordlist)
    
    return solvedWordlist


def checkAround(maxRows, maxColumns, board, currentRow, currentColumn, word, wordNumber, solvedWordlist):

    # generates -1, 0, 1 for both x and y to check surrounding 8 characters
    for rowOffset in range(-1, 2):
        newRow = currentRow + rowOffset

        for columnOffset in range(-1, 2):
            newColumn = currentColumn + columnOffset

            # ensure only checking indexes in range and not original character
            if 0 <= newRow and newRow < maxRows and \
                0 <= newColumn and newColumn < maxColumns and \
                (rowOffset != 0 or columnOffset != 0):

                # if second character matches, continue in that direction to match entire word
                if word[1] == board[newRow][newColumn]:
                    followWord(maxRows, maxColumns, board, currentRow, currentColumn, rowOffset, columnOffset, \
                        word, wordNumber, solvedWordlist)


def followWord(maxRows, maxColumns, board, currentRow, currentColumn, rowOffset, columnOffset, word, wordNumber, solvedWordlist):
    wordLength = len(word)

    for offsetMultiplier in range(1, wordLength):
        
        # adjust magnitude of offset based on which character to check
        newRow = currentRow + rowOffset * offsetMultiplier
        newColumn = currentColumn + columnOffset * offsetMultiplier

        # ensure indexes are in range
        if 0 <= newRow and newRow < maxRows and 0 <= newColumn and newColumn < maxColumns:
            if word[offsetMultiplier] == board[newRow][newColumn]:

                # if entire word matches, append its location to solved word list
                if offsetMultiplier == wordLength - 1:
                    solvedWordlist.append("%s %s %s:%s %s:%s"%(wordNumber, word, currentRow, currentColumn, newRow, newColumn))
            else:
                break


def main():
    filename = input("Input the file name: ")

    solvedWordlist = list()

    totalRows, totalColumns, board, wordlist = importWordSearch(filename)
    solveWordSearch(totalRows, totalColumns, board, wordlist, solvedWordlist)

    # sort list based on word number
    for solvedWord in sorted(solvedWordlist):

        # output only word and location
        print(solvedWord.split(' ', 1)[1])


if __name__ == "__main__":
    main()